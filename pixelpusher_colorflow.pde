import com.heroicrobot.dropbit.registry.*;
import com.heroicrobot.dropbit.devices.pixelpusher.Pixel;
import com.heroicrobot.dropbit.devices.pixelpusher.Strip;
import java.util.*;
import controlP5.*;

DeviceRegistry registry;

class TestObserver implements Observer {
  public boolean hasStrips = false;
  public void update(Observable registry, Object updatedDevice) {
    println("Registry changed!"); 
    if (updatedDevice != null) {
      println("Device change: " + updatedDevice);
    }
    this.hasStrips = true;
  }
}

TestObserver testObserver;

int MIN_COLOR = 40;
int MAX_COLOR = 256;
int currentRed, currentGreen, currentBlue;
int targetRed, targetGreen, targetBlue;
int nextRed, nextGreen, nextBlue;
color[] pix;
int step = 0;
StringList order;
ControlP5 cp5;
boolean forward = true;

void setup() {
  registry = new DeviceRegistry();
  testObserver = new TestObserver();
  registry.addObserver(testObserver);
  registry.setAntiLog(true);
  registry.setLogging(false);
  registry.setAutoThrottle(false);
  
  frameRate(30);
  order = new StringList();
  pix = new color[240];
  
  initColor();
  
  cp5 = new ControlP5(this);
  cp5.addBang("REVERSE").setPosition(20, 20).setSize(60, 60);
}

void draw() {
  if (testObserver.hasStrips) {   
    registry.startPushing();
    List<Strip> strips = registry.getStrips();
    int numStrips = strips.size();
    if (numStrips == 0) return;
    
    if (!forward) {
      step = (step+1)%240;
    }
    
    for(Strip strip : strips) {
      for (int stripx = 0; stripx < strip.getLength(); stripx++) {
        strip.setPixel(pix[((stripx+step)%240)], stripx);
      }
    }
    
    if (forward) {
      step = (step+239)%240;
    }
    
    morphColor();
    pix[step] = color(currentRed, currentGreen, currentBlue);
  }
}

void REVERSE() {
  forward = !forward;
}

private void channelShuffle() {
  order.clear();
  order.append("red");
  order.append("green");
  order.append("blue");
  order.shuffle();
  order.append("end");
}

private void initColor() {
  currentRed = int(random(MIN_COLOR, MAX_COLOR));
  currentGreen = int(random(MIN_COLOR, MAX_COLOR));
  currentBlue = int(random(MIN_COLOR, MAX_COLOR));
  targetRed = int(random(MIN_COLOR, MAX_COLOR));
  targetGreen = int(random(MIN_COLOR, MAX_COLOR));
  targetBlue = int(random(MIN_COLOR, MAX_COLOR));
  nextRed = int(random(MIN_COLOR, MAX_COLOR));
  nextGreen = int(random(MIN_COLOR, MAX_COLOR));
  nextBlue = int(random(MIN_COLOR, MAX_COLOR));
  
  for (int i = 0; i < 240; i++) {
    pix[i] = color(currentRed, currentGreen, currentBlue);
  }
  
  channelShuffle();
}

private void morphColor() {
  if ("red".equals(order.get(0))) {
    if (currentRed < targetRed) currentRed++;
    if (currentRed > targetRed) currentRed--;
    if (currentRed == targetRed) order.remove(0);
  } else if ("green".equals(order.get(0))) {
    if (currentGreen < targetGreen) currentGreen++;
    if (currentGreen > targetGreen) currentGreen--;
    if (currentGreen == targetGreen) order.remove(0);
  } else if ("blue".equals(order.get(0))) {
    if (currentBlue < targetBlue) currentBlue++;
    if (currentBlue > targetBlue) currentBlue--;
    if (currentBlue == targetBlue) order.remove(0);
  }
  if ("end".equals(order.get(0))) {
    nextColor();
  }
}

private void nextColor() {
  targetRed = nextRed;
  targetGreen = nextGreen;
  targetBlue = nextBlue;
  nextRed = int(random(MIN_COLOR, MAX_COLOR));
  nextGreen = int(random(MIN_COLOR, MAX_COLOR));
  nextBlue = int(random(MIN_COLOR, MAX_COLOR));
  
  channelShuffle();
}